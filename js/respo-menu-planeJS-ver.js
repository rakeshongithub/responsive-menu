;(function(){
		
		// Below code will work IE9+ only
		var mainNav = document.querySelector('#menu'),
			cloneNav = mainNav.cloneNode(true),
			childUl = cloneNav.querySelector("ul"),
			childLi = childUl.children,
			select = document.getElementById("devi-menu");
			
			for(var i = 0; i < childLi.length; i++){

				var navHref = childLi[i].children[0].getAttribute("href"),
					navText = childLi[i].children[0].textContent,
					subLevel = childLi[i].children;

				select.innerHTML += "<option value='"+navHref+"'>"+navText+"</option>";

				if(subLevel.length > 0){

					for(var j = 0; j < subLevel.length; j++){

						if(subLevel[j].tagName.toLowerCase() === "ul"){
							var subLevelLi = subLevel[j].children,
								len = subLevelLi.length;

							for(var k = 0; k < len; k++){

								if(subLevelLi[k].children[0] !== undefined){

									var subNavHref = subLevelLi[k].children[0].getAttribute("href"),
										subNavText = subLevelLi[k].children[0].textContent;

										select.innerHTML += "<option value='"+subNavHref+"'>---"+subNavText+"</option>";
								};
							};
						};

					};
				};
			};

			select.onchange = function(){
								// console.log(this)
									if(this.options[this.selectedIndex].value !== "#"){
										location = this.options[this.selectedIndex].value;
									};
								};
}())
